<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Infrastructure;

use BNNVARA\AkamaiClient\Application\DtoMapperInterface;
use BNNVARA\AkamaiClient\Application\Encoder\EncoderInterface;
use BNNVARA\AkamaiClient\Application\Mapper\SingleFieldDtoMapperInterface;
use BNNVARA\AkamaiClient\Domain\AkamaiClientInterface;
use BNNVARA\AkamaiClient\Domain\DtoInterface;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidResponseException;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;
use BNNVARA\AkamaiClient\Domain\LiteAccountDto;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;
use Nyholm\Psr7\Request;
use Psr\Http\Client\ClientInterface;

class AkamaiClient implements AkamaiClientInterface
{
    protected ClientInterface $client;
    protected DtoMapperInterface $mapper;
    private SingleFieldDtoMapperInterface $singleFieldDtoMapper;
    protected EncoderInterface $encoder;

    protected string $authenticationHash;
    protected string $userEntityName;

    const STAT_OK_MESSAGE = 'ok';

    public function __construct(
        ClientInterface $client,
        DtoMapperInterface $accountMapper,
        SingleFieldDtoMapperInterface $singleFieldDtoMapper,
        EncoderInterface $encoder,
        string $apiKey,
        string $apiSecret,
        string $userEntityName
    ) {
        $this->client = $client;
        $this->mapper = $accountMapper;
        $this->singleFieldDtoMapper = $singleFieldDtoMapper;
        $this->encoder = $encoder;

        $this->authenticationHash = $this->encoder->encode($apiKey . ':' . $apiSecret);
        $this->userEntityName = $userEntityName;
    }

    /**
     * @todo this createLiteAccount function probably will need it's own, seperate, client in the future, for now, we make it part of the normal client.
     *  This because it should not be possible to create a Lite account while using the Hosted Login entity, which theoretically is possible now.
     */
    public function createLiteAccount(LiteAccountDto $accountDto): void
    {
        $urlParameters = $this->mapper->mapAccountDtoToSingleAccountString($accountDto, false);
        $endpoint = '/entity.create?' . $urlParameters;
        $this->doCurlRequest($endpoint);
    }

    public function updateAccount(DtoInterface $accountDto): void
    {
        $urlParameters = $this->mapper->mapAccountDtoToSingleAccountString($accountDto, true);
        $endpoint = '/entity.update?' . $urlParameters;
        $this->doCurlRequest($endpoint);
    }

    public function getAccountByAccountId(string $accountId): DtoInterface
    {
        $endpoint = sprintf('entity?type_name=%s&uuid=%s', $this->userEntityName, $accountId);
        return $this->getAccountFromSingleResultEndpoint($endpoint);
    }

    public function getAccountByExternalId(string $externalId): DtoInterface
    {
        $endpoint = 'entity.find?type_name=' . $this->userEntityName . '&filter=externalId%3D%27' . $externalId . '%27';
        return $this->getAccountFromMultipleResultsEndpoint($endpoint);
    }

    public function getAccountByEmailAddress(string $emailAddress): DtoInterface
    {
        $endpoint = 'entity.find?type_name=' . $this->userEntityName . '&filter=email%3D%27' . $emailAddress . '%27';
        return $this->getAccountFromMultipleResultsEndpoint($endpoint);
    }

    public function getAccountByUsername(string $username): DtoInterface
    {
        $endpoint = 'entity.find?type_name=' . $this->userEntityName . '&filter=username%3D%27' . $username . '%27';
        return $this->getAccountFromMultipleResultsEndpoint($endpoint);
    }

    public function getAccountByRelationNumber(string $relationNumber): DtoInterface
    {
        $endpoint = 'entity.find?type_name=' . $this->userEntityName . '&filter=relationNumber%3D%27' . $relationNumber . '%27';
        return $this->getAccountFromMultipleResultsEndpoint($endpoint);
    }

    public function deleteByAccountId(string $accountid): void
    {
        $endpoint = sprintf('entity.delete?type_name=%s&uuid=%s', $this->userEntityName, $accountid);
        $this->doCurlRequest($endpoint);
    }

    public function getAmount(): int
    {
        $endpoint = sprintf('entity.count?type_name=%s', $this->userEntityName);
        $akamaiResponseObject = json_decode($this->doCurlRequest($endpoint));

        return $akamaiResponseObject->total_count;
    }

    public function updateSingleField(SingleFieldDtoInterface $singleFieldDto): void
    {
        $urlParameters = $this->singleFieldDtoMapper->mapSingleFieldToSingleAccountString($singleFieldDto);
        $endpoint = sprintf('/entity.update?%s', $urlParameters);
        $this->doCurlRequest($endpoint);
    }

    public function getSingleFieldByAccountId(string $accountId): SingleFieldDtoInterface
    {
        $endpoint = sprintf('/entity?type_name=%s&uuid=%s', $this->userEntityName, $accountId);

        return $this->singleFieldDtoMapper->mapSingleAccountJsonToSingleFieldDto($this->doCurlRequest($endpoint));
    }

    private function doCurlRequest(string $endpoint): string
    {
        $akamaiAccountJson = $this->client->sendRequest(
            new Request(
                'POST',
                $endpoint,
                [
                        'Authorization' => 'Basic ' . $this->authenticationHash
                ]
            )
        )->getBody()
            ->getContents();

        $akamaiResponseObject = json_decode($akamaiAccountJson);

        if ($akamaiResponseObject->stat !== self::STAT_OK_MESSAGE) {
            throw new InvalidResponseException("Response is not ok: " . $akamaiAccountJson);
        }

        return $akamaiAccountJson;
    }

    private function getAccountFromSingleResultEndpoint(string $endpoint): DtoInterface
    {
        return $this->mapper->mapSingleAccountJsonToSingleAccountDto($this->doCurlRequest($endpoint));
    }

    private function getAccountFromMultipleResultsEndpoint(string $endpoint): DtoInterface
    {
        $akamaiAccountJson = $this->doCurlRequest($endpoint);
        $akamaiResponseObject = json_decode($akamaiAccountJson);

        if (property_exists($akamaiResponseObject, 'results')) {
            if (count($akamaiResponseObject->results) === 0) {
                throw new UnableToRetrieveAccountException("Account could not be found");
            }
        } else {
            throw new UnableToRetrieveAccountException("Unexpected response from Akamai");
        }

        return $this->mapper->mapMultipleAccountJsonToSingleAccountDto($akamaiAccountJson);
    }
}
