<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain;

use phpseclib3\Crypt\Common\PublicKey;

class JsonWebKeyDto
{
    private string $id;
    private string $algorithm;
    private PublicKey $rsa;

    public function __construct(
        string $id,
        string $algorithm,
        PublicKey $rsa
    ) {
        $this->id = $id;
        $this->algorithm = $algorithm;
        $this->rsa = $rsa;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function getRSA(): PublicKey
    {
        return $this->rsa;
    }
}
