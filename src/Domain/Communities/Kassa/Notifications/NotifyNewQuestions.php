<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications;

class NotifyNewQuestions extends Notification
{
    public const NAME = 'notifyNewQuestions';
}
