<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class CarsTransport extends Category
{
    public const NAME = 'cars_transport';
}
