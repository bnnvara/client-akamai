<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

use ArrayObject;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityCollectionInterface;
use BNNVARA\AkamaiClient\Domain\Communities\CommunityElementInterface;

class CategoryCollection extends ArrayObject implements CommunityCollectionInterface
{
    public function add(CommunityElementInterface $element): void
    {
        parent::append($element);
    }
}
