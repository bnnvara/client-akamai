<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

use BNNVARA\AkamaiClient\Domain\Communities\CommunityElementInterface;

abstract class Category implements CommunityElementInterface
{
    private bool $value = false;

    public function getValue(): bool
    {
        return $this->value;
    }

    public function setValue(bool $value): void
    {
        $this->value = $value;
    }
}
