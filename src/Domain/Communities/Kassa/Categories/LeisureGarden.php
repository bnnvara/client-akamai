<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class LeisureGarden extends Category
{
    public const NAME = 'leisure_garden';
}
