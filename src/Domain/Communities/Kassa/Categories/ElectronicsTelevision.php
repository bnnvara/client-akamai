<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class ElectronicsTelevision extends Category
{
    public const NAME = 'electronics_television';
}
