<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class ComputersTelecom extends Category
{
    public const NAME = 'computers_telecom';
}
