<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class WarrantyInsurance extends Category
{
    public const NAME = 'warranty_insurance';
}
