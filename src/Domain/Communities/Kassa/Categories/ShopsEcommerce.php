<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class ShopsEcommerce extends Category
{
    public const NAME = 'shops_ecommerce';
}
