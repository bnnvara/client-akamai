<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class HousekeepingEnergy extends Category
{
    public const NAME = 'housekeeping_energy';
}
