<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class FoodHealth extends Category
{
    public const NAME = 'food_health';
}
