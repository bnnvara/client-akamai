<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories;

class MoneyLaw extends Category
{
    public const NAME = 'money_law';
}
