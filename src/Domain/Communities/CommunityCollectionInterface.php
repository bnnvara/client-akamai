<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities;

interface CommunityCollectionInterface
{
    public function add(CommunityElementInterface $element): void;
}
