<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Communities;

use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;

class CommunitiesDto implements SingleFieldDtoInterface
{
    private string $accountId;
    private CommunityDto $kassa;
    private CommunityDto $vroegevogels;

    public function __construct(
        string $accountId,
        CommunityDto $kassa,
        CommunityDto $vroegevogels
    ) {
        $this->accountId = $accountId;
        $this->kassa = $kassa;
        $this->vroegevogels = $vroegevogels;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getKassa(): CommunityDto
    {
        return $this->kassa;
    }

    public function getVroegevogels(): CommunityDto
    {
        return $this->vroegevogels;
    }
}
