<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain;

interface SingleFieldDtoInterface
{
    public function getAccountId(): string;
}
