<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain;

interface DtoInterface
{
    public function getEmail(): string;

    public function getPrimaryAddressStreetName(): string;

    public function getPrimaryAddressHouseNumber(): string;

    public function getPrimaryAddressHouseNumberAddition(): string;

    public function getPrimaryAddressCountry(): string;

    public function getPrimaryAddressCity(): string;

    public function getPrimaryAddressZip(): string;

    public function getContactNumber(): string;

    public function getRelationNumber(): string;

    public function getAccountId(): ?string;
}
