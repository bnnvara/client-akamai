<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain;

class AuthenticatedUserDto
{
    private string $accountId;

    public function __construct(string $accountId)
    {
        $this->accountId = $accountId;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }
}
