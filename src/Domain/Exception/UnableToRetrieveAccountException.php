<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class UnableToRetrieveAccountException extends Exception
{
}
