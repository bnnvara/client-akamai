<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class InvalidNumberOfAccountsException extends Exception
{
}
