<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Domain\Exception;

use Exception;

class InvalidJsonException extends Exception
{
}
