<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Decoder\Exception;

use Exception;

class ExpiredJsonWebTokenException extends Exception
{

}
