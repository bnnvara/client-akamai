<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Decoder;

interface DecoderInterface
{
    public function decode(string $stringToDecode): string;
}
