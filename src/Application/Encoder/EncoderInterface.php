<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Encoder;

interface EncoderInterface
{
    public function encode(string $stringToEncode): string;
}
