<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Validator;

interface ValidatorInterface
{
    public function validate(string $string): bool;
}
