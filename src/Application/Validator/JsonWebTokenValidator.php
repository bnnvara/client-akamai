<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Validator;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use Exception;

class JsonWebTokenValidator implements ValidatorInterface
{
    private JsonWebKeyDtoCollection $jsonWebKeys;
    private DecoderInterface $decoder;
    private DecoderInterface $sanitizedDecoder;

    const JWT_HEADER_INDEX = 0;
    const JWT_PAYLOAD_INDEX = 1;
    const JWT_SIGNATURE_INDEX = 2;

    public function __construct(
        JsonWebKeyDtoCollection $jsonWebKeys,
        DecoderInterface $decoder,
        DecoderInterface $sanitizedDecoder
    ) {
        $this->jsonWebKeys = $jsonWebKeys;
        $this->decoder = $decoder;
        $this->sanitizedDecoder = $sanitizedDecoder;
    }

    public function validate(string $string): bool
    {
        $this->validateString($string);

        $deconstructedToken = $this->deconstructToken($string);

        $header = $deconstructedToken[self::JWT_HEADER_INDEX];
        $payload = $deconstructedToken[self::JWT_PAYLOAD_INDEX];
        $signature = $deconstructedToken[self::JWT_SIGNATURE_INDEX];

        $keyId = $this->getKeyIdFromHeader($header);
        $message = $this->buildMessageFromHeaderAndPayload($header, $payload);
        $base64DecodedAndSanitizedSignature = $this->sanitizedDecoder->decode($signature);

        $key = $this->jsonWebKeys->getById($keyId);
        $rsa = $key->getRSA();

        $tokenIsValid = false;

        try {
            $tokenIsValid = $rsa->verify($message, $base64DecodedAndSanitizedSignature);
        } catch (Exception $e) {
            // token will remain false if an exception is thrown
        }

        return $tokenIsValid;
    }

    private function validateString(string $string): void
    {
        if (preg_match("~^[A-Za-z0-9-_]+\.{1}[A-Za-z0-9-_]+\.{1}[A-Za-z0-9-_]+$~", $string) !== 1) {
            throw new InvalidJsonWebTokenException('JSON Web Token is structured incorrectly');
        }
    }

    private function getKeyIdFromHeader(string $header): string
    {
        $decodedHeader = $this->decoder->decode($header);
        $headerObject = json_decode($decodedHeader);

        if (!isset($headerObject->kid)) {
            throw new InvalidJsonWebTokenException('Invalid token');
        }

        return $headerObject->kid;
    }

    private function buildMessageFromHeaderAndPayload(string $header, string $payload): string
    {
        return sprintf('%s.%s', $header, $payload);
    }

    private function deconstructToken(string $jsonWebToken): array
    {
        return explode('.', $jsonWebToken);
    }
}
