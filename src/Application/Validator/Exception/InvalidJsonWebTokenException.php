<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Validator\Exception;

use Exception;

class InvalidJsonWebTokenException extends Exception
{

}
