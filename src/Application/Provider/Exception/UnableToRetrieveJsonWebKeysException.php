<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Provider\Exception;

use Exception;

class UnableToRetrieveJsonWebKeysException extends Exception
{

}
