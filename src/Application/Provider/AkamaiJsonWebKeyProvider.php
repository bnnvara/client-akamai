<?php

declare(strict_types=1);

namespace BNNVARA\AkamaiClient\Application\Provider;

use BNNVARA\AkamaiClient\Application\Factory\JsonWebKeyDtoCollectionFactoryInterface;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use Nyholm\Psr7\Request;
use Psr\Http\Client\ClientInterface;

class AkamaiJsonWebKeyProvider implements JsonWebKeyProviderInterface
{
    private ClientInterface $client;
    private JsonWebKeyDtoCollectionFactoryInterface $factory;

    public function __construct(
        ClientInterface $client,
        JsonWebKeyDtoCollectionFactoryInterface $factory
    ) {
        $this->client = $client;
        $this->factory = $factory;
    }

    public function getJsonWebKeys(): JsonWebKeyDtoCollection
    {
        $result = $this->doCurlRequest();

        $collection = $this->factory->build($result);

        return $collection;
    }

    private function doCurlRequest(): string
    {
        return $this->client->sendRequest(new Request('GET', 'jwk'))->getBody()->getContents();
    }
}
