<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain\Communities\Kassa;

use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewAnswers;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewQuestions;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotifyNewReactions;
use PHPUnit\Framework\TestCase;

class NotificationCollectionTest extends TestCase
{
    /** @test */
    public function notificationsCanBeAddedToTheCollection()
    {
        $collection = new NotificationCollection();
        $collection->add(new NotifyNewAnswers(true));
        $collection->add(new NotifyNewQuestions(true));
        $collection->add(new NotifyNewReactions(true));

        $this->assertCount(3, $collection);
    }
}
