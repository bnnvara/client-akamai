<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain;

use BNNVARA\AkamaiClient\Domain\LiteAccountDto;
use PHPUnit\Framework\TestCase;

class LiteAccountDtoTest extends TestCase
{
    /** @test */
    public function aLiteAccountDtoCanBeCreated(): void
    {
        $subscription1 = new \stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = '2020-05-11 10:00:00 +0100';
        $subscription1->name = 'DWDD';

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = 'teststraat';
        $primaryAddressHouseNumber = '1';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = 'nederland';
        $primaryAddressCity = 'teststad';
        $primaryAddressZip = '1234AB';
        $contactNumber = '948d450d-266f-44be-91b0-63ea4de3bbbb';
        $relationNumber = '12345678';

        $account = new LiteAccountDto(
            '12345678-1234-1234-1234-123456789012',
            '2020-05-11 10:00:00 +0100',
            'test@example.com',
            '2020-05-12 10:00:00 +0100',
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $contactNumber,
            $relationNumber,
            $subscriptions
        );

        $this->assertInstanceOf(LiteAccountDto::class, $account);
        $this->assertSame('12345678-1234-1234-1234-123456789012', $account->getAccountId());
        $this->assertSame('2020-05-11 10:00:00 +0100', $account->getCreatedDateTime());
        $this->assertSame('test@example.com', $account->getEmail());
        $this->assertSame('2020-05-12 10:00:00 +0100', $account->getLastUpdated());

        $this->assertSame($primaryAddressStreetName, $account->getPrimaryAddressStreetName());
        $this->assertSame($primaryAddressHouseNumber, $account->getPrimaryAddressHouseNumber());
        $this->assertSame($primaryAddressHouseNumberAddition, $account->getPrimaryAddressHouseNumberAddition());
        $this->assertSame($primaryAddressCountry, $account->getPrimaryAddressCountry());
        $this->assertSame($primaryAddressCity, $account->getPrimaryAddressCity());
        $this->assertSame($primaryAddressZip, $account->getPrimaryAddressZip());
        $this->assertSame($contactNumber, $account->getContactNumber());
        $this->assertSame($relationNumber, $account->getRelationNumber());

        $this->assertCount(1, $account->getSubscriptions()->newsletters);
        $this->assertSame('Confirmed', $account->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertNotNull($account->getSubscriptions()->newsletters[0]->updated);
        $this->assertSame('DWDD', $account->getSubscriptions()->newsletters[0]->name);
        $this->assertSame('2563', $account->getSubscriptions()->newsletters[0]->id);
    }

    /** @test */
    public function aLiteAccountDtoWithNoAccountIdCanBeCreated(): void
    {
        $subscription1 = new \stdClass();
        $subscription1->id = '2563';
        $subscription1->doubleOptInStatus = 'Confirmed';
        $subscription1->updated = '2020-05-11 10:00:00 +0100';
        $subscription1->name = 'DWDD';

        $subscriptions = new \stdClass();
        $subscriptions->newsletters = [$subscription1];

        $primaryAddressStreetName = 'teststraat';
        $primaryAddressHouseNumber = '1';
        $primaryAddressHouseNumberAddition = '';
        $primaryAddressCountry = 'nederland';
        $primaryAddressCity = 'teststad';
        $primaryAddressZip = '1234AB';
        $contactNumber = '948d450d-266f-44be-91b0-63ea4de3bbbb';
        $relationNumber = '12345678';

        $account = new LiteAccountDto(
            null,
            '2020-05-11 10:00:00 +0100',
            'test@example.com',
            '2020-05-12 10:00:00 +0100',
            $primaryAddressStreetName,
            $primaryAddressHouseNumber,
            $primaryAddressHouseNumberAddition,
            $primaryAddressCountry,
            $primaryAddressCity,
            $primaryAddressZip,
            $contactNumber,
            $relationNumber,
            $subscriptions
        );

        $this->assertInstanceOf(LiteAccountDto::class, $account);
        $this->assertNull($account->getAccountId());
        $this->assertSame('2020-05-11 10:00:00 +0100', $account->getCreatedDateTime());
        $this->assertSame('test@example.com', $account->getEmail());
        $this->assertSame('2020-05-12 10:00:00 +0100', $account->getLastUpdated());

        $this->assertSame($primaryAddressStreetName, $account->getPrimaryAddressStreetName());
        $this->assertSame($primaryAddressHouseNumber, $account->getPrimaryAddressHouseNumber());
        $this->assertSame($primaryAddressHouseNumberAddition, $account->getPrimaryAddressHouseNumberAddition());
        $this->assertSame($primaryAddressCountry, $account->getPrimaryAddressCountry());
        $this->assertSame($primaryAddressCity, $account->getPrimaryAddressCity());
        $this->assertSame($primaryAddressZip, $account->getPrimaryAddressZip());
        $this->assertSame($contactNumber, $account->getContactNumber());
        $this->assertSame($relationNumber, $account->getRelationNumber());

        $this->assertCount(1, $account->getSubscriptions()->newsletters);
        $this->assertSame('Confirmed', $account->getSubscriptions()->newsletters[0]->doubleOptInStatus);
        $this->assertNotNull($account->getSubscriptions()->newsletters[0]->updated);
        $this->assertSame('DWDD', $account->getSubscriptions()->newsletters[0]->name);
        $this->assertSame('2563', $account->getSubscriptions()->newsletters[0]->id);
    }
}
