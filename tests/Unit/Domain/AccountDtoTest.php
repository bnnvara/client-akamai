<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Domain;

use PHPUnit\Framework\TestCase;
use Tests\BNNVARA\AkamaiClient\Helpers\AccountDtoFactory;

class AccountDtoTest extends TestCase
{
    /** @test */
    public function anAccountDtoCanBeCreated(): void
    {
        $definition = AccountDtoFactory::createAccountDefinition();
        $account = AccountDtoFactory::createFullAccountDto($definition);

        $this->assertSame($definition['accountId'], $account->getAccountId());
        $this->assertSame($definition['givenName'], $account->getGivenName());
        $this->assertSame($definition['registrationSource'], $account->getRegistrationSource());
        $this->assertSame($definition['bio'], $account->getBio());
        $this->assertSame($definition['isVerified'], $account->isVerified());
        $this->assertSame($definition['isRegistered'], $account->isRegistered());
        $this->assertSame($definition['isLockedOut'], $account->isLockedOut());
        $this->assertSame($definition['isBanned'], $account->isBanned());
        $this->assertSame($definition['isActive'], $account->isActive());
        $this->assertSame($definition['isLite'], $account->isLite());
        $this->assertSame($definition['emails'], $account->getEmails());
        $this->assertSame($definition['email'], $account->getEmail());
        $this->assertSame($definition['emailVerified'], $account->getEmailVerified());
        $this->assertSame($definition['lastUpdated'], $account->getLastUpdated());
        $this->assertSame($definition['middleName'], $account->getMiddleName());
        $this->assertSame($definition['firstContact'], $account->getFirstContact());
        $this->assertSame($definition['primaryAddressStreetname'], $account->getPrimaryAddressStreetname());
        $this->assertSame($definition['primaryAddressHousenumber'], $account->getPrimaryAddressHousenumber());
        $this->assertSame($definition['primaryAddressHousenumberAddition'], $account->getPrimaryAddressHousenumberAddition());
        $this->assertSame($definition['primaryAddressCountry'], $account->getPrimaryAddressCountry());
        $this->assertSame($definition['primaryAddressCity'], $account->getPrimaryAddressCity());
        $this->assertSame($definition['primaryAddressZip'], $account->getPrimaryAddressZip());
        $this->assertSame($definition['relationNumber'], $account->getRelationNumber());
        $this->assertSame($definition['hasMarketingConsent'], $account->getHasMarketingConsent());
        $this->assertSame($definition['marketingConsentUpdatedDateTime'], $account->getMarketingConsentUpdatedDateTime());
        $this->assertSame($definition['hasTrackingConsent'], $account->getHasTrackingConsent());
        $this->assertSame($definition['trackingConsentUpdatedDateTime'], $account->getTrackingConsentUpdatedDateTime());
        $this->assertSame($definition['birthYear'], $account->getBirthYear());
        $this->assertSame($definition['birthMonth'], $account->getBirthMonth());
        $this->assertSame($definition['birthDay'], $account->getBirthDay());
        $this->assertSame($definition['createdDateTime'], $account->getCreatedDateTime());
        $this->assertSame($definition['username'], $account->getUsername());
        $this->assertSame($definition['familyName'], $account->getFamilyName());
        $this->assertSame($definition['password'], $account->getPassword());
        $this->assertSame($definition['phones'], $account->getPhones());
        $this->assertSame($definition['photos'], $account->getPhotos());
        $this->assertSame($definition['displayName'], $account->getDisplayName());
        $this->assertSame($definition['externalId'], $account->getExternalId());
        $this->assertSame($definition['contactNumber'], $account->getContactNumber());
        $this->assertSame($definition['legalAcceptances'], $account->getLegalAcceptances());
        $this->assertSame($definition['gender'], $account->getGender());
        $this->assertSame($definition['subscriptions'], $account->getSubscriptions());
        $this->assertSame($definition['communities'], $account->getCommunities());
    }

    /** @test */
    public function theAvatarIsItsMainPicture(): void
    {
        $definition = AccountDtoFactory::createAccountDefinition();
        $definition['photos'] = [
            (object) [
                'value' => 'https://example.com/not-the-avatar.jpg',
                'type' => 'not-this-one',
            ],
            (object) [
                'value' => 'https://example.com/avatar.jpg',
                'type' => 'avatar',
            ],
        ];
        $account = AccountDtoFactory::createFullAccountDto($definition);

        $this->assertSame($definition['photos'][1]->value, $account->getPicture());
    }

    /** @test */
    public function itMayNotHaveAPicture(): void
    {
        $definition = AccountDtoFactory::createAccountDefinition();
        $definition['photos'] = [
            (object) [
                'value' => 'https://example.com/not-the-avatar.jpg',
                'type' => 'not-this-one',
            ],
        ];
        $account = AccountDtoFactory::createFullAccountDto($definition);

        $this->assertNull($account->getPicture());
    }

    /** @test */
    public function subscriptionsMayChange(): void
    {
        $definition = AccountDtoFactory::createAccountDefinition();
        $definition['subscriptions'] = (object) [];
        $account = AccountDtoFactory::createFullAccountDto($definition);

        $newSubscriptions = (object) [
            'newsletters' => [],
        ];
        $account->setSubscriptions($newSubscriptions);

        $this->assertSame($newSubscriptions, $account->getSubscriptions());
    }
}
