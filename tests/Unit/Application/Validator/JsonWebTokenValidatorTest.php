<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Validator;

use BNNVARA\AkamaiClient\Application\Decoder\DecoderInterface;
use BNNVARA\AkamaiClient\Application\Validator\Exception\InvalidJsonWebTokenException;
use BNNVARA\AkamaiClient\Application\Validator\JsonWebTokenValidator;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use Exception;
use phpseclib3\Crypt\Hash;
use phpseclib3\Crypt\Common\PublicKey;
use PHPUnit\Framework\TestCase;

class RSAMockedClass {
    private $returnValueForVerify;
    private $expectVerifyToThrowException;

    public function __construct(bool $expectVerifyToReturn = true, bool $expectVerifyToThrowException = false) {
        $this->returnValueForVerify = $expectVerifyToReturn;
        $this->expectVerifyToThrowException = $expectVerifyToThrowException;
    }

    public function verify() {
        if ($this->expectVerifyToThrowException) {
            throw new Exception('Invalid signature');
        }

        return $this->returnValueForVerify;
    }
}

class RSAAdapter implements PublicKey {
    private RSAMockedClass $rsa;

    public function __construct(RSAMockedClass $rsa)
    {
        $this->rsa = $rsa;
    }

    public function verify($message, $signature)
    {
        return $this->rsa->verify();
    }
    public function toString($type, array $options = [])
    {
        return 'string';
    }
    public function getFingerprint($algorithm)
    {
        $hash = new Hash('sha256');
        $base = base64_encode($hash->hash('key'));

        return substr($base, 0, -1);
    }
}

class JsonWebTokenValidatorTest extends TestCase
{
    /** @test */
    public function jsonWebTokenIsConsideredValidWhenValidatedAgainstJsonWebKeys(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithMultipleKeys(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64Decoder()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.pay_load.c2lnbmF0dXJl
TOKEN;

        $this->assertTrue($validator->validate($jsonWebToken));
    }

    /** @test */
    public function invalidJsonWebTokenIsRejected(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithDtoThatReturnsFalseUponVerification(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64Decoder()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload.c2lnbmF0dXJl
TOKEN;

        $this->assertFalse($validator->validate($jsonWebToken));
    }

    /** @test */
    public function jsonWebTokenWithAnInvalidSignatureIsRejected(): void
    {
        $validator = new JsonWebTokenValidator(
            $this->getJsonWebKeyDtoCollectionWithDtoThatThrowsAnExceptionBecauseTheSignatureIsInvalid(),
            $this->getBase64Decoder(),
            $this->getSanitizedBase64DecoderThatReturnsAnInvalidSignature()
        );

        $jsonWebToken = <<<TOKEN
eyJhbGciOiJSUzI1NiIsImtpZCI6IjNjNTE0MzYyN2Y3NDFiOGVmM2ZhOWFmNWExODdhZGM3OGQwMDQ1MzgiLCJ0eXAiOiJKV1QifQ.payload.aW52YWxpZF9zaWduYXR1cmU
TOKEN;

        $this->assertFalse($validator->validate($jsonWebToken));
    }

    /**
     * @test
     * @dataProvider invalidInputProvider
     */
    public function anExceptionIsThrownIfInputIsStructuredIncorrectly(string $input): void
    {
        $this->expectException(InvalidJsonWebTokenException::class);

        $validator = new JsonWebTokenValidator(
            new JsonWebKeyDtoCollection(),
            $this->getMockBuilder(DecoderInterface::class)->getMock(),
            $this->getMockBuilder(DecoderInterface::class)->getMock()
        );

        $validator->validate($input);
    }

    /**
     * @test
     */
    public function anExceptionIsThrownIfHeaderIsMissingTheKeyId(): void
    {
        $this->expectException(InvalidJsonWebTokenException::class);

        $validator = new JsonWebTokenValidator(
            new JsonWebKeyDtoCollection(),
            $this->getMockBuilder(DecoderInterface::class)->getMock(),
            $this->getMockBuilder(DecoderInterface::class)->getMock()
        );

        $validator->validate('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.payload.signature');
    }

    private function getJsonWebKeyDtoCollectionWithMultipleKeys(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = new RSAMockedClass();
        $rsaAdapter = new RSAAdapter($rsa);

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsaAdapter);

        $collection->add($dto);

        $dto = new JsonWebKeyDto('94d6ee7a5afaf393956a1631f3b610d1fb64dc9f', 'RS256', $rsaAdapter);

        $collection->add($dto);

        return $collection;
    }

    private function getJsonWebKeyDtoCollectionWithDtoThatReturnsFalseUponVerification(): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = new RSAMockedClass(false);
        $rsaAdapter = new RSAAdapter($rsa);

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsaAdapter);

        $collection->add($dto);

        return $collection;
    }

    private function getJsonWebKeyDtoCollectionWithDtoThatThrowsAnExceptionBecauseTheSignatureIsInvalid(
    ): JsonWebKeyDtoCollection
    {
        $collection = new JsonWebKeyDtoCollection();

        $rsa = new RSAMockedClass(false, true);
        $rsaAdapter = new RSAAdapter($rsa);

        $dto = new JsonWebKeyDto('3c5143627f741b8ef3fa9af5a187adc78d004538', 'RS256', $rsaAdapter);

        $collection->add($dto);

        return $collection;
    }

    private function getBase64Decoder(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $header = <<<HEADER
{
  "alg": "RS256",
  "kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
  "typ": "JWT"
}
HEADER;

        $decoder->expects($this->once())->method('decode')->willReturn($header);

        return $decoder;
    }

    private function getSanitizedBase64Decoder(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $decoder->expects($this->once())->method('decode')->willReturn('signature');

        return $decoder;
    }

    private function getSanitizedBase64DecoderThatReturnsAnInvalidSignature(): DecoderInterface
    {
        $decoder = $this->getMockBuilder(DecoderInterface::class)->getMock();

        $decoder->expects($this->once())->method('decode')->willReturn('invalid_signature');

        return $decoder;
    }

    public function invalidInputProvider(): array
    {
        return array(
            array(
                'your'
            ),
            array(
                'json.web'
            ),
            array(
                't.o.k.e.n'
            ),
            array(
                'i.s..'
            ),
            array(
                'invalid '
            )
        );
    }
}
