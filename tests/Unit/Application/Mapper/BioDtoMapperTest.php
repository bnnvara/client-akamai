<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Mapper;

use BNNVARA\AkamaiClient\Application\Mapper\BioDtoMapper;
use BNNVARA\AkamaiClient\Application\Mapper\CommunitiesDtoMapper;
use BNNVARA\AkamaiClient\Domain\BioDto;
use BNNVARA\AkamaiClient\Domain\Communities\CommunitiesDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\Category;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Categories\CategoryCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\KassaDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\Notification;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\Notifications\NotificationCollection;
use BNNVARA\AkamaiClient\Domain\Communities\Vroegevogels\VroegevogelsDto;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;
use PHPUnit\Framework\TestCase;
use stdClass;
use Tests\BNNVARA\AkamaiClient\Fixtures\KassaPreferencesFixture;

class BioDtoMapperTest extends TestCase
{
    /** @test */
    public function itMapsABioDtoToASingleAccountString(): void
    {
        $bio = $this->getBioDto();

        $mapper = new BioDtoMapper('userHL');

        $string = $mapper->mapSingleFieldToSingleAccountString($bio);

        $this->assertSame(
            file_get_contents(__DIR__ . '/../../../Fixtures/singleAccountStringForBio.txt'),
            $string
        );
    }

    /** @test */
    public function itMapsASingleAccountJsonWithABioFieldThatContainsAStringValueToABioDto(): void
    {
        $account = new stdClass();
        $account->result = new stdClass();
        $account->result->uuid = '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd';
        $account->result->bio = 'Hello my name is...';

        $singleAccountString = json_encode($account);

        $mapper = new BioDtoMapper('userHL');

        $dto = $mapper->mapSingleAccountJsonToSingleFieldDto($singleAccountString);

        $this->assertInstanceOf(BioDto::class, $dto);

        $this->assertSame('0a25ffb6-da73-4aba-8d1f-52f78e07c3dd', $dto->getAccountId());
        $this->assertIsString($dto->getValue());
        $this->assertSame('Hello my name is...', $dto->getValue());
    }

    /** @test */
    public function itMapsASingleAccountJsonWithABioFieldThatContainsANullValueToABioDto(): void
    {
        $account = new stdClass();
        $account->result = new stdClass();
        $account->result->uuid = '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd';
        $account->result->bio = null;

        $singleAccountString = json_encode($account);

        $mapper = new BioDtoMapper('userHL');

        $dto = $mapper->mapSingleAccountJsonToSingleFieldDto($singleAccountString);

        $this->assertInstanceOf(BioDto::class, $dto);

        $this->assertSame('0a25ffb6-da73-4aba-8d1f-52f78e07c3dd', $dto->getAccountId());
        $this->assertNull($dto->getValue());
    }

    public function getBioDto(): SingleFieldDtoInterface
    {
        return new BioDto(
            '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd',
            'Hello my name is...'
        );
    }
}
