<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Functional\Application\Provider;

use BNNVARA\AkamaiClient\Application\Factory\JsonWebKeyDtoCollectionFactoryInterface;
use BNNVARA\AkamaiClient\Application\Provider\AkamaiJsonWebKeyProvider;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDto;
use BNNVARA\AkamaiClient\Domain\JsonWebKeyDtoCollection;
use phpseclib3\Crypt\RSA;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class AkamaiJsonWebKeyProviderTest extends TestCase
{
    /** @test */
    public function itProvidesACollectionOfJsonWebKeyDtos(): void
    {
        $client = $this->getClient();
        $factory = $this->getJsonWebKeyRSAFactory();

        $provider = new AkamaiJsonWebKeyProvider($client, $factory);

        /** @var JsonWebKeyDtoCollection $keys */
        $keys = $provider->getJsonWebKeys();

        $this->assertInstanceOf(JsonWebKeyDtoCollection::class, $keys);
        $this->assertCount(1, $keys);
        $this->assertInstanceOf(JsonWebKeyDto::class, $keys[0]);
        $this->assertEquals($keys[0]->getId(), '3c5143627f741b8ef3fa9af5a187adc78d004538');
        $this->assertInstanceOf(RSA::class, $keys[0]->getRSA());
    }

    private function getClient(): ClientInterface
    {
        $json = <<<JSON
{
	"keys": [{
		"use": "sig",
		"kty": "RSA",
		"kid": "3c5143627f741b8ef3fa9af5a187adc78d004538",
		"alg": "RS256",
		"n": "Pz8_Pz8_Pz8_Pz8",
		"e": "YWJjZGVmZw"
	}]
}
JSON;

        $emptyResponse = json_encode([
                                         'results' => json_decode($json, true),
                                         'stat' => 'ok'
                                     ]);

        $bodyMock = $this->getMockBuilder(StreamInterface::class)->getMock();
        $bodyMock->expects($this->once())->method('getContents')->willReturn($emptyResponse);

        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->expects($this->once())->method('getBody')->willReturn($bodyMock);

        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        $client->expects($this->once())->method('sendRequest')->willReturn($response);

        /** @var $client ClientInterface */
        return $client;
    }

    private function getJsonWebKeyRSAFactory(): JsonWebKeyDtoCollectionFactoryInterface
    {
        $factory = $this->getMockBuilder(JsonWebKeyDtoCollectionFactoryInterface::class)->getMock();

        $collection = new JsonWebKeyDtoCollection();

        $jsonWebKey = new JsonWebKeyDto(
            '3c5143627f741b8ef3fa9af5a187adc78d004538',
            'RS256',
            RSA::createKey()->getPublicKey()
        );
        $collection->add($jsonWebKey);

        $factory->expects($this->once())->method('build')->willReturn($collection);

        return $factory;
    }
}
