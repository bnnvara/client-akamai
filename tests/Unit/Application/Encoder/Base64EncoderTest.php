<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Application\Encoder;

use BNNVARA\AkamaiClient\Application\Encoder\Base64Encoder;
use PHPUnit\Framework\TestCase;

class Base64EncoderTest extends TestCase
{
    /** @test
     * @dataProvider getTestStrings
     */
    public function aStringCanBeBase64Encoded(string $stringToEncode, string $encodedString): void
    {
        $encoder = new Base64Encoder();
        $this->assertEquals($encodedString, $encoder->encode($stringToEncode));
    }

    public function getTestStrings(): array
    {
        return [
            [
                'testString',
                'dGVzdFN0cmluZw=='
            ],
            [
                'anotherstring1234',
                'YW5vdGhlcnN0cmluZzEyMzQ='
            ],
            [
                '!@#$%^&**()";:.><,/?',
                'IUAjJCVeJioqKCkiOzouPjwsLz8='
            ],
        ];
    }
}
