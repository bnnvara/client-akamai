<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Unit\Infrastructure;

use BNNVARA\AkamaiClient\Application\DtoMapperInterface;
use BNNVARA\AkamaiClient\Application\Encoder\EncoderInterface;
use BNNVARA\AkamaiClient\Application\Mapper\SingleFieldDtoMapperInterface;
use BNNVARA\AkamaiClient\Domain\Communities\CommunitiesDto;
use BNNVARA\AkamaiClient\Domain\Communities\Kassa\KassaDto;
use BNNVARA\AkamaiClient\Domain\Communities\Vroegevogels\VroegevogelsDto;
use BNNVARA\AkamaiClient\Domain\Exception\InvalidResponseException;
use BNNVARA\AkamaiClient\Domain\Exception\UnableToRetrieveAccountException;
use BNNVARA\AkamaiClient\Domain\SingleFieldDtoInterface;
use BNNVARA\AkamaiClient\Infrastructure\AkamaiClient;
use Nyholm\Psr7\Request;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Tests\BNNVARA\AkamaiClient\Fixtures\KassaPreferencesFixture;

class AkamaiClientTest extends TestCase
{
    use KassaPreferencesFixture;

    /** @test */
    public function anExceptionIsThrownWhenGettingAnAccountByAccountIdFails(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $client = new AkamaiClient(
            $this->createUnableToRetrieveAccountExceptionThrowingClient(),
            $this->createDtoMapperInterface(),
            $this->createMockedSingleFieldDtoMapperInterface(),
            $this->createEncoderInterface(),
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAccountByAccountId('accountId');
    }

    /** @test */
    public function anExceptionIsThrownWhenANonExistingAccountIsDeleted(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $httpClient = $this->createUnableToRetrieveAccountExceptionThrowingClient();
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->deleteByAccountId('accountId');
    }

    /** @test */
    public function anExceptionIsThrownWhenGettingAnAccountByEmailAddressFails(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $httpClient = $this->createUnableToRetrieveAccountExceptionThrowingClient();
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAccountByEmailAddress('emailAddress');
    }

    /** @test */
    public function anExceptionIsThrownWhenGettingAnAccountByExternalIdFails(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $httpClient = $this->createUnableToRetrieveAccountExceptionThrowingClient();
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAccountByExternalId('xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx');
    }

    /** @test */
    public function anExceptionIsThrownWhenAmountOfEntitiesCouldNotBeRetrieved(): void
    {
        $this->expectException(InvalidResponseException::class);

        $httpClient = $this->createClientWithResponse(
            $this->createResponseWithBody(
                $this->createStreamWithContent(
                    json_encode(
                        [
                            'results' => [],
                            'stat' => 'anything_but_ok'
                        ]
                    )
                )
            )
        );
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAmount();
    }

    /** @test */
    public function anExceptionIsThrownWhenGettingAnEmptyResultResponse(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $httpClient = $this->createClientWithResponse(
            $this->createResponseWithBody(
                $this->createStreamWithContent(
                    json_encode(
                        [
                            'results' => [],
                            'stat' => 'ok'
                        ]
                    )
                )
            )
        );
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAccountByExternalId('xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx');
    }

    /** @test */
    public function anExceptionIsThrownWhenGettingABadResultResponse(): void
    {
        $this->expectException(UnableToRetrieveAccountException::class);

        $httpClient = $this->createClientWithResponse(
            $this->createResponseWithBody(
                $this->createStreamWithContent(
                    json_encode(
                        [
                            'stat' => 'ok'
                        ]
                    )
                )
            )
        );
        $mapper = $this->createDtoMapperInterface();
        $communitiesMapper = $this->createMockedSingleFieldDtoMapperInterface();
        $encoder = $this->createEncoderInterface();

        $client = new AkamaiClient(
            $httpClient,
            $mapper,
            $communitiesMapper,
            $encoder,
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $client->getAccountByExternalId('xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx');
    }

    /** @test */
    public function communitiesFieldIsUpdated(): void
    {
        $client = new AkamaiClient(
            $this->createClientWithEntityUpdateCall(
                file_get_contents(__DIR__ . '/../../Fixtures/singleAccountStringForCommunities.txt')
            ),
            $this->createDtoMapperInterface(),
            $this->getMockedCommunitiesMapperThatReturnsASingleAccountString(),
            $this->getMockedEncoderThatIsExpectedToReturnAnAuthenticationHash(),
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $categories = $this->getCategoriesThatAreAllSetToTrue();
        $notifications = $this->getNotificationsThatAreAllSetToTrue();

        $communities = new CommunitiesDto(
            '0a25ffb6-da73-4aba-8d1f-52f78e07c3dd',
            new KassaDto($categories, $notifications),
            new VroegevogelsDto()
        );

        $client->updateSingleField($communities);
    }

    /** @test */
    public function communitiesFieldIsRetrieved(): void
    {
        $uuid = '4ab3ffbb-6c13-4b71-b52c-34bb2b9e185a';

        $client = new AkamaiClient(
            $this->createClientRetrievingDataForSingleUser($uuid),
            $this->createDtoMapperInterface(),
            $this->getMockedCommunitiesMapperThatMapsAnASingleAccountStringToACommunitiesDto(),
            $this->getMockedEncoderThatIsExpectedToReturnAnAuthenticationHash(),
            'apiKey',
            'apiSecret',
            'userHL'
        );

        $communities = $client->getSingleFieldByAccountId($uuid);

        $this->assertInstanceOf(SingleFieldDtoInterface::class, $communities);
    }

    private function createClientWithEntityUpdateCall(
        string $string
    ): ClientInterface {
        $response = $this->createResponseWithBody(
            $this->createStreamWithContent(
                json_encode(
                    [
                        'stat' => 'ok'
                    ]
                )
            )
        );

        /** @var MockObject $client */
        $client = $this->createClientInterface();
        $client->expects($this->once())
            ->method('sendRequest')
            ->with(
                new Request(
                    'POST',
                    sprintf('/entity.update?%s', $string),
                    [
                            'Authorization' => 'Basic hash'
                    ]
                )
            )
            ->willReturn($response);

        /** @var ClientInterface $client */
        return $client;
    }

    private function createClientRetrievingDataForSingleUser(
        string $uuid
    ): ClientInterface {
        $response = $this->createResponseWithBody(
            $this->createStreamWithContent(
                json_encode(
                    [
                        'stat' => 'ok'
                    ]
                )
            )
        );

        /** @var MockObject $client */
        $client = $this->createClientInterface();

        $client->expects($this->once())->method('sendRequest')->with(
            new Request(
                'POST',
                sprintf('/entity?type_name=userHL&uuid=%s', $uuid),
                [
                        'Authorization' => 'Basic hash'
                ]
            )
        )->willReturn($response);

        /** @var ClientInterface $client */
        return $client;
    }

    private function getMockedEncoderThatIsExpectedToReturnAnAuthenticationHash(): EncoderInterface
    {
        $encoder = $this->getMockBuilder(EncoderInterface::class)->getMock();

        $encoder->expects($this->once())
            ->method('encode')
            ->with('apiKey:apiSecret')
            ->willReturn('hash');

        /** @var EncoderInterface $encoder */
        return $encoder;
    }

    private function createEncoderInterface(): EncoderInterface
    {
        $encoder = $this->getMockBuilder(EncoderInterface::class)->getMock();

        /** @var EncoderInterface $encoder */
        return $encoder;
    }

    private function createUnableToRetrieveAccountExceptionThrowingClient(): ClientInterface
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();

        $client->expects($this->once())
            ->method('sendRequest')
            ->willThrowException(new UnableToRetrieveAccountException());

        /** @var $client ClientInterface */
        return $client;
    }

    private function createClientWithResponse(ResponseInterface $response): ClientInterface
    {
        $client = $this->createClientInterface();
        $client->expects($this->once())->method('sendRequest')->willReturn($response);

        /** @var $client ClientInterface */
        return $client;
    }

    private function createDtoMapperInterface(): DtoMapperInterface
    {
        $mapper = $this->getMockBuilder(DtoMapperInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var DtoMapperInterface $mapper */
        return $mapper;
    }

    private function createMockedSingleFieldDtoMapperInterface(): SingleFieldDtoMapperInterface
    {
        $mapper = $this->getMockBuilder(SingleFieldDtoMapperInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var SingleFieldDtoMapperInterface $mapper */
        return $mapper;
    }

    private function getMockedCommunitiesMapperThatReturnsASingleAccountString(): SingleFieldDtoMapperInterface
    {
        $mapper = $this->getMockBuilder(SingleFieldDtoMapperInterface::class)->getMock();

        $mapper->expects($this->once())->method('mapSingleFieldToSingleAccountString')->willReturn(
            file_get_contents(__DIR__ . '/../../Fixtures/singleAccountStringForCommunities.txt')
        );

        return $mapper;
    }

    private function getMockedCommunitiesMapperThatMapsAnASingleAccountStringToACommunitiesDto(
    ): SingleFieldDtoMapperInterface
    {
        $mapper = $this->getMockBuilder(SingleFieldDtoMapperInterface::class)->getMock();

        $mapper->expects($this->once())->method('mapSingleAccountJsonToSingleFieldDto')->willReturn(
            $this->getMockBuilder(CommunitiesDto::class)->disableOriginalConstructor()->getMock()
        );

        return $mapper;
    }

    private function createStreamWithContent(string $content): StreamInterface
    {
        /** @var MockObject $stream */
        $stream = $this->createStreamInterface();
        $stream->expects($this->once())->method('getContents')->willReturn($content);

        /** @var StreamInterface $stream */
        return $stream;
    }

    private function createStreamInterface(): StreamInterface
    {
        $stream = $this->getMockBuilder(StreamInterface::class)->getMock();

        /** @var StreamInterface $stream */
        return $stream;
    }

    private function createResponseInterface(): ResponseInterface
    {
        /** @var MockObject $responseMock */
        $responseMock = $this->getMockBuilder(ResponseInterface::class)->getMock();

        /** @var ResponseInterface */
        return $responseMock;
    }

    private function createResponseWithBody(StreamInterface $stream): ResponseInterface
    {
        /** @var MockObject $response */
        $response = $this->createResponseInterface();
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        /** @var ResponseInterface $responseMock */
        return $response;
    }

    private function createClientInterface(): ClientInterface
    {
        $client = $this->getMockBuilder(ClientInterface::class)->getMock();
        return $client;
    }
}
