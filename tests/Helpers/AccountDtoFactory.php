<?php

declare(strict_types=1);

namespace Tests\BNNVARA\AkamaiClient\Helpers;

use BNNVARA\AkamaiClient\Domain\AccountDto;

class AccountDtoFactory
{
    /**
     * @return array<string, mixed>
     */
    public static function createAccountDefinition(): array
    {
        return [
            'accountId' => '12345678-1234-1234-1234-123456789012',
            'givenName' => 'Test',
            'middleName' => 'de',
            'familyName' => 'Persoon',
            'username' => 'testpersoon',
            'displayName' => 'test',
            'bio' => 'Ik ben de tester',
            'isVerified' => true,
            'isRegistered' => true,
            'isLockedOut' => false,
            'isBanned' => false,
            'isActive' => true,
            'isLite' => false,
            'emails' => ['testman@bnnvara.nl'],
            'email' => 'testman@bnnvara.nl',
            'emailVerified' => 'verified',
            'lastUpdated' => '2020-05-11 10:00:00 +0100',
            'firstContact' => '2020-05-10 10:00:00 +0100',
            'primaryAddressStreetname' => 'teststraat',
            'primaryAddressHousenumber' => '1',
            'primaryAddressHousenumberAddition' => '',
            'primaryAddressCountry' => 'nederland',
            'primaryAddressCity' => 'teststad',
            'primaryAddressZip' => '1234AB',
            'hasMarketingConsent' => false,
            'marketingConsentUpdatedDateTime' => '',
            'hasTrackingConsent' => false,
            'trackingConsentUpdatedDateTime' => '',
            'birthYear' => '1970',
            'birthMonth' => '1',
            'birthDay' => '1',
            'createdDateTime' => '2020-05-09 10:00:00 +0100',
            'registrationSource' => 'registrationSource',
            'password' => null,
            'phones' => [],
            'photos' => [],
            'externalId' => '08aba0-987-44b595d4d02827cec99c029bf7c',
            'contactNumber' => '948d450d-266f-44be-91b0-63ea4de3bbbb',
            'relationNumber' => '123456-324-234-78',
            'legalAcceptances' => [],
            'gender' => 'male',
            'subscriptions' => (object) [
                'newsletters' => [
                    (object) [
                        'id' => '2563',
                        'doubleOptInStatus' => 'Confirmed',
                        'updated' => '2020-05-11 10:00:00 +0100',
                        'name' => 'DWDD',
                    ],
                ],
            ],
            'communities' => (object) [
                'kassa' => (object) [],
            ],
            'picture' => 'https://example.com/testpicture.png',
        ];
    }

    public static function createFullAccountDto(array $definition = []): AccountDto
    {
        return new AccountDto(...array_values($definition));
    }
}
