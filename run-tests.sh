#!/bin/sh

set -eu

composer install

XDEBUG_MODE=coverage vendor/bin/phpunit -c phpunit.xml.dist --stop-on-failure --fail-on-empty-test-suite --log-junit ./var/reports/phpunit.junit.xml --coverage-clover=./var/reports/phpunit.coverage.xml